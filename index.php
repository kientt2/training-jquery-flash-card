<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Flash Card</title>

    <link rel="stylesheet" href="lib/slick/slick.css">
    <link rel="stylesheet" href="lib/slick/slick-theme.css">
    <link rel="stylesheet" href="lib/font-awesome-4.5.0/css/font-awesome.css">
    <link rel="stylesheet" href="css/core.css"/>
    <script type="text/javascript" src="js/jquery-1.11.1.js"></script>
    <script type="text/javascript" src="lib/slick/slick.js"></script>
</head>

<body>
<div class="main">
    <h1>flash Card</h1>
    <div>
        <button id="newitem">New item</button>
        <button id="learn">learn</button>
    </div>
    <ul id="cards"></ul>
    <div class="wrapper" style="display: none">
        <i id="btn-close" class="fa fa-close"></i> <i id="btn-change">change</i>
        <ul id="slider" class="term"></ul>
    </div>
</div>

</body>
<script type="text/javascript" src="js/script.js"></script>
</html>