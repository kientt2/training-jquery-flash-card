<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta meaning="viewport" content="width=device-width, initial-scale=1">
    <title>Flash Card</title>
    <link rel="stylesheet" href="css/core.css"/>
    <script type="text/javascript" src="js/jquery-1.11.1.js"></script>
</head>

<style>




</style>

<body>

<div class="main">


    <h1>flash Card</h1>
    <div>
        <button id="newitem">New item</button>
    </div>
    <ul id="cards"></ul>
</div>

</body>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript">
    var test = $.ajax({
        url: "api/api.php",
        type: "get",
        success: function(data){
            console.log(data);
        },
        error: function(){
            alert("err");
        }
    });

    var cards = [
        {
            term: "CA",
            meaning: "C�p T?n"
        },
        {
            term: "HK",
            meaning: "H�c"
        },
        {
            term: "IM",
            meaning: "Ai R?n Man"
        },
    ];

    var cardContainer = $("#cards");
    function FlashCard ( container, cards ) {
        this.container = container;
        this.cards = cards;
        this.count = 0;
    }
    FlashCard.prototype.createNode = function( obj ){
        this.count++;
        var that = this;
        var item = $("<li/>");

        var isNewItem = false;
        if( !obj){
            isNewItem = true;
            item.addClass("editing");
            obj = {
                term: "",
                meaning: ""
            };
        }


        item.append( $("<span/>", {
            class: "card-number",
            text: this.count
        }));
        var cardTerm = $("<span/>", {
            class: "card-term",
            text: obj.term
        });
        var cardTermInput = $("<input/>", {
            class: "card-term-input",
            type: "text",
            value: obj.term
        });

        var cardMeaning = $("<span/>", {
            class: "card-meaning",
            text: obj.meaning
        });
        var cardMeaningInput = $("<input/>", {
            class: "card-meaning-input",
            type: "text",
            value: obj.meaning
        });

        var buttonContainer = $("<div/>", {
            class: "buttons"
        });
        var btnSubmit = $("<span/>", {
            class: "btn-submit",
            text: "done"
        });
        var btnEdit = $("<span/>", {
            class: "btn-edit",
            text: "edit"
        });
        var btnRemove =  $("<span/>", {
            class: "btn-remove",
            text: "remove"
        });
        btnSubmit.on("click", function(){
            cardTerm.html(cardTermInput.val());
            cardMeaning.html(cardMeaningInput.val());
            item.removeClass("editing");
            $("#newitem").removeAttr("disabled");
        });
        btnEdit.on("click", function(){
            item.addClass("editing");
            $("#newitem").attr("disabled", "");
        });
        btnRemove.on("click", function(){
            item.remove();
        });

        item.append(cardTerm );
        item.append(cardTermInput );
        item.append(cardMeaning);
        item.append(cardMeaningInput);
        buttonContainer.append(btnSubmit);
        buttonContainer.append(btnEdit);
        buttonContainer.append(btnRemove);
        item.append(buttonContainer);

        return item;
    };
    FlashCard.prototype.render = function () {
        var that = this;
        that.cards.forEach(function( obj ){
            that.container.append( that.createNode(obj) );
        });

        $("#newitem").on("click", function(){
            this.disabled = true;
            that.container.append(that.createNode())
        });
    };


    /*begin*/
//    var flashcard = new FlashCard( $("#cards"), cards );
//    flashcard.render();




</script>
</html>