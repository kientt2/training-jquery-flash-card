<?php

require '../Slim/Slim.php';
require '../class/JsonDB.class.php';

$db = new JsonDB("./data/");
$table = 'cards';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

// GET:: list of cards
$app->get('/cards', function () {
    global $db, $table;
    $app = \Slim\Slim::getInstance();
    
    // execute query
    $result = $db->selectAll($table);
    
    // response data
    $app->response()->header("Content-Type", "application/json");
    echo json_encode($result);
});

// GET:: card details
$app->get('/card/:id', function ($id) {
    global $db, $table;
    $app = \Slim\Slim::getInstance();
    
    // prepare data
    $result = $db->select($table,"id",$id);
    
    $app->response()->header("Content-Type", "application/json");
    echo json_encode($result);
});

// POST:: insert new card
$app->post('/card', function () {
    global $db, $table;
    $app = \Slim\Slim::getInstance();
    
    // prepare data
    $data = $app->request->post();
    $data['id'] = rand(100,100000000);
    
    // execute query
    $result = $db->insert($table, $data);
    
    // response data
    $app->response()->header("Content-Type", "application/json");
    echo json_encode($data);
});


// PUT:: update new card
$app->put('/card/:id', function ($id) {
    global $db, $table;
    $app = \Slim\Slim::getInstance();
    
    // prepare data
    $data = $app->request->put();
    $data['id'] = $id;
    
    // execute query
    $result = $db->update($table, "id", $id, $data);
    
    // response data
    $app->response()->header("Content-Type", "application/json");
    echo json_encode($data);
});

// DELETE:: delete card
$app->delete('/card/:id', function ($id) {
    global $db, $table;
    $app = \Slim\Slim::getInstance();
    
    // get deleted card
    $result = $db->select($table,"id",$id);
    
    // execute query
    $db->delete($table, "id", $id);
    
    // response data
    $app->response()->header("Content-Type", "application/json");
    echo json_encode($result);
});


$app->run();

?>