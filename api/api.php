<?php

/**
 * Acts as a basic router for RESTful CRUD requests.
 */

$cards = array(
    array(
        "term" => "CA",
        "meaning" => "C�p T?n"
    ),
    array(
        "term" => "HK",
        "meaning" => "H�c"
    ),
    array(
        "term" => "IM",
        "meaning" => "Ai R?n Man"
    ),
);
$out = array_values($cards);
json_encode($out);

//$cards = array(
//    "term" => "IM",
//    "meaning" => "Ai R?n Man"
//);

//echo "<pre>";
//var_dump( $cards); die;

try {
    $method = _getMethod();
//    $model = json_decode($_REQUEST['model']);
//    $params = _getParams($method, $model);

    switch ($method) {

        case "GET": // Read
            // attempt to retrieve value from WP cache.
            // if(!empty($params['EmailAddress'])){
            //   $resp = get_transient($params['EmailAddress']);
            // }
            // cache miss. get from ET
            // if($resp === false){


            $resp = array(
                "type" => "POST",
            );
            $resp = $cards;
            // }
            // save response from WP cache.
            // if(!empty($resp->Status)){
            // set_transient($params['EmailAddress'], $resp, 1 * DAY_IN_SECONDS);
            // }
            break;
        case "POST": // Create
            $resp = array(
                "type" => "POST",
            );
            break;
        case "PUT": // Update
            $resp = array(
                "type" => "PUT",
            );
            break;
        case "DELETE": // Destroy
            $resp = array(
                "type" => "DELETE",
            );
            break;
        default:
//            $resp = $params;
    }

    //if ($error) header('HTTP/1.1 422 Unprocessable Entity');
    echo json_encode( $resp );

} catch (Exception $e) {
    /*
      Deliver response back to page
    */
    echo json_encode(array(
        "OverallStatus" => false,
        "message" => $e
    ));
    die();
}

/**
 * Private functions used here
 */

function _getMethod(){
    $headers = array();
    foreach (getallheaders() as $key => $value) {
        $headers[$key] = $value;
    }
    // $method = $_SERVER['REQUEST_METHOD'] == 'GET' ? 'GET' : $headers['X-HTTP-Method-Override'];
    $method = $_SERVER['REQUEST_METHOD'] == 'GET' ? 'GET' : $_SERVER['REQUEST_METHOD'];
    return $method;
}

function _getParams($method, $model){
    $params = array();
    if($method == 'GET'){
        foreach ($_GET as $key => $value) {
            $params[$key] = $value;
        }
    }
    elseif($method == 'DELETE' || $method == 'PUT'){
        parse_str(file_get_contents('php://input'), $params);
        $model = json_decode($params['model']);
        foreach ($model as $key => $value) {
            $params[$key] = $value;
        }
        unset($params['model']);
    }
    else{
        foreach ($_POST as $key => $value) {
            $params[$key] = $value;
        }
    }

    return $params;
}