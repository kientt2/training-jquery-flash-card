var App = {
    getJson: function(){
        $.ajax({
            url: "api/g1/cards",
            type: "GET",
            async: false,
            success: function (data) {
                res = data
            },
            error: function () {
                alert("Error! Cannot get data.");
            }
        });
        this.json = res;
    },
    renderCards: function(){
        if (this.json.length == 0) {
            alert(" bug. no json found !");
            return;
        }
        this.json.forEach(function (obj) {
            $("#cards").append( new FlashCard( obj ) );
        });
        $("#newitem").on("click", function () {
            $("#cards").prepend(new FlashCard());
            $("#cards .new-item .card-term-input").focus();
        });
    },
    learnPopup: function(){
        $("#btn-close").on("click", function () {
            $(".wrapper").hide();
            $("#slider").slick("unslick");
        });
        $("#btn-change").on("click", function () {
            $("#slider").toggleClass("toggle");
        });
        $("#slider").on("touchend", function(){
            $("#slider").toggleClass("toggle");
        });
        $("#learn").on("click", function () {
            var html = $("#cards").html();
            $("#slider").html($("#cards").html());
            $(".wrapper").show();
            $("#slider ").slick();
        });
    }
};

function FlashCard(obj) {
    this.main = $(".main");
    var that = this;
    var Card = this;
    this.obj = obj;
    this.item = $("<li/>");
    var isNewItem = false;
    if (!this.obj) {
        isNewItem = true;
        this.item.addClass("new-item");
        Card.toggleState("editing", Card.item);
        this.obj = {
            term: "",
            meaning: ""
        };
    }
    this.item.append($("<span/>", {
        class: "card-number",
        text: this.obj.id
    }));
    this.cardTerm = $("<span/>", {
        class: "card-term",
        text: this.obj.term
    });
    this.cardTermInput = $("<input/>", {
        class: "card-term-input",
        type: "text",
        value: this.obj.term
    });
    this.cardMeaning = $("<span/>", {
        class: "card-meaning",
        text: this.obj.meaning
    });
    this.cardMeaningInput = $("<input/>", {
        class: "card-meaning-input",
        type: "text",
        value: this.obj.meaning
    });
    this.buttonContainer = $("<div/>", {
        class: "buttons"
    });
    this.btnSubmit = $("<span/>", {
        class: "btn-submit fa fa-check-circle",
        title: "OK"
    });
    this.btnEdit = $("<span/>", {
        class: "btn-edit fa fa-edit",
        title: "Edit Card"
    });
    this.btnRemove = $("<span/>", {
        class: "btn-remove fa fa-remove",
        title: "Remove Card"
    });
    $(this.cardMeaningInput, this.cardTermInput).each(function () {
        $(this).keydown(function (e) {
            if (e.keyCode == 13) {
                Card.btnSubmit.click();
            }
        });
    });
    Card.btnSubmit.on("click", $.proxy(Card.btnSubmitFunc, this) );
    Card.btnEdit.on("click", $.proxy(Card.btnEditFunc, this));
    Card.btnRemove.on("click", $.proxy(Card.btnRemoveFunc, this));
    Card.item.append(Card.cardTerm);
    Card.item.append(Card.cardTermInput);
    Card.item.append(Card.cardMeaning);
    Card.item.append(Card.cardMeaningInput);
    Card.buttonContainer.append(Card.btnSubmit);
    Card.buttonContainer.append(Card.btnEdit);
    Card.buttonContainer.append(Card.btnRemove);
    Card.item.append(Card.buttonContainer);
    return Card.item;
}
FlashCard.prototype.btnEditFunc = function(){
    this.toggleState("editing", this.item);
    this.cardMeaningInput.val( this.cardMeaning.html() );
    this.cardTermInput.val( this.cardTerm.html() );
    this.cardTermInput.focus();
    this.btnRemove.attr("title", "Cancel");
};
FlashCard.prototype.btnRemoveFunc = function() {
    var Card = this;
    if( Card.item.hasClass("current")){
        if( Card.item.hasClass("new-item")){
            Card.toggleState("stop", Card.item);
            Card.item.remove();
        } else {
            Card.toggleState("stop", Card.item);
            Card.btnRemove.attr("title", "Remove Card");
        }
    } else {
        if (window.confirm("Are you sure ?")) {
            Card.toggleState("processing");
            $.ajax({
                url: "api/g1/card/" + Card.obj.id,
                type: "DELETE",
                async: false,
                success: function (data) {
                    Card.item.fadeOut("slow", function(){ Card.item.remove() });
                    Card.toggleState("unprocessing");
                }
            });
        }
    }
};
FlashCard.prototype.btnSubmitFunc = function(){
    var Card = this;
    if( Card.cardTermInput.val() == ""  ){
        Card.cardTermInput.focus();
        return;
    }
    if(Card.cardMeaningInput.val() == ""){
        Card.cardMeaningInput.focus();
        return;
    }
    Card.cardTerm.html(Card.cardTermInput.val());
    Card.cardMeaning.html(Card.cardMeaningInput.val());
    Card.toggleState("processing");
    if (Card.item.hasClass("new-item")) {
        $.ajax({
            url: "api/g1/card",
            type: "POST",
            data: {term: Card.cardTermInput.val(), meaning: Card.cardMeaningInput.val()},
            async: false,
            success: function (data) {
                $(".current .card-number").html(data.id);
                $(".new-item").removeClass("new-item").appendTo($("#cards"));
                Card.toggleState("unprocessing");
            }
        });
    }
    else {
        $.ajax({
            url: "api/g1/card/" + Card.obj.id,
            type: "PUT",
            data: {term: Card.cardTermInput.val(), meaning: Card.cardMeaningInput.val()},
            async: false,
            success: function (data) {
                $(".current .card-number").html(data.id);
                Card.cardTerm.html(Card.cardTermInput.val());
                Card.cardMeaning.html(Card.cardMeaningInput.val());
                Card.toggleState("unprocessing");
            }
        });
    }
    Card.toggleState("stop", Card.item);
    Card.btnRemove.attr("title", "Remove Card");
};
FlashCard.prototype.toggleState = function(state, item){
    var Card = this;
    if( state == "editing"){
        Card.main.addClass("editing");
        if(item){
            item.addClass("current");
        }
    } else if ( state == "stop") {
        this.main.removeClass("editing");
        if(item){
            item.removeClass("current");
        }
    } else if ( state == "processing") {
        this.main.addClass("processing");
    } else if ( state == "unprocessing"){
        this.main.removeClass("processing");
    }
};



/*====BEGIN===*/
App.getJson();
App.renderCards();
App.learnPopup();

